#include "opengl.h"
#include "common.h"

void _opengl_error(const char *filename, int line) {
    GLenum err;
    while ((err = glGetError()) != GL_NO_ERROR) {
        log_error("error %x at %s line %d", err, filename, line);
    }
}