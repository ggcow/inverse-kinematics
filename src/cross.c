#include "cross.h"

cross_t * cross_create()
{
    cross_t *cross = callocate(sizeof *cross);

    if (!(cross->renderer = renderer_create(
            "/shaders/cross.vs",
            "/shaders/cross.fs"
    ))) return NULL;

    float w = .04f, h = .04f;
    GLfloat data[] = {
            w*.1f, -h, w*.1f, h,
            -w*.1f, h, -w*.1f, -h,
            w, -h*.1f, w, h*.1f,
            -w, h*.1f, -w, -h*.1f
    };

    glBindBuffer(GL_ARRAY_BUFFER, cross->renderer->vbo);
    glBindVertexArray(cross->renderer->vao);
    glEnableVertexAttribArray(0);
    glVertexAttribFormat(0, 2, GL_FLOAT, GL_FALSE, 0);
    glVertexAttribBinding(0, 0);
    glBufferData(GL_ARRAY_BUFFER, sizeof data, data, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    return cross;
}

void cross_destroy(cross_t *cross)
{
    renderer_destroy(cross->renderer);
    deallocate(cross);
}

void cross_draw(cross_t *cross, f32 x, f32 y)
{
    glBindVertexArray(cross->renderer->vao);
    glUseProgram(cross->renderer->shader_program->program);
    glBindBuffer(GL_ARRAY_BUFFER, cross->renderer->vbo);
    glBindVertexBuffer(0, cross->renderer->vbo, 0, 2 * sizeof(GLfloat));
    glUniform2f(1, x, y);
//    log_info("cross : %f %f", x, y);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glUseProgram(0);
    glBindVertexArray(0);
}