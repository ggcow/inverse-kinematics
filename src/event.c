#include "event.h"

event_t poll_events(window_t *window, i32 *x, i32 *y)
{
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_WINDOWEVENT:
                if (event.window.event == SDL_WINDOWEVENT_CLOSE)
                    return (event_t) {EVENT_QUIT, event.window.windowID};
                else return (event_t) {EVENT_WINDOW, event.window.windowID};
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym) {
                    case SDLK_ESCAPE:
                    case SDLK_KP_ENTER:
                    case SDLK_RETURN:
                        return (event_t) {EVENT_QUIT, event.key.windowID};
                    case SDLK_p:
                        return (event_t) {EVENT_P, event.key.windowID};
                    case SDLK_m:
                        return (event_t) {EVENT_M, event.key.windowID};
                }
                break;
            case SDL_MOUSEMOTION:
                if (window->id == event.motion.windowID) {
                    if (x) *x = event.motion.x;
                    if (y) *y = event.motion.y;
                }
                break;
            case SDL_MOUSEBUTTONDOWN:
                if (event.button.button == SDL_BUTTON_LEFT)
                    return (event_t) {EVENT_MAP, event.button.windowID};
        }
    }
    return (event_t) {.action=0};
}
