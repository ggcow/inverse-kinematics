#include "map.h"

#define omp

static void map_save_texture(map_t *map)
{
    window_gl_acquire(map->window);
    glBindTexture(GL_TEXTURE_2D, map->texture);
    glTexSubImage2D(
            GL_TEXTURE_2D,0,
            0, 0, map->window->width, map->window->height,
            GL_RGB, GL_FLOAT, map->data
    );
    glBindTexture(GL_TEXTURE_2D, 0);
    window_gl_release(map->window);
}

void map_resize(map_t *map)
{
    deallocate(map->data);
    map->data = allocate(3 * sizeof *map->data * map->window->width * map->window->height);
    window_gl_acquire(map->window);
    glBindTexture(GL_TEXTURE_2D, map->texture);
    glTexImage2D(
            GL_TEXTURE_2D,0,
            GL_RGB, map->window->width, map->window->height,
            0, GL_RGB, GL_FLOAT, NULL
    );
    glBindTexture(GL_TEXTURE_2D, 0);
    window_gl_release(map->window);
}

void map_refresh(map_t *map, joint_t *head, buffer_block_t buffer)
{
    memset(map->data, 0, sizeof *map->data * 3 * map->window->width * map->window->height);

    GLfloat white[] = {1, 1, 1};
#ifdef omp
#pragma omp parallel for default(none) shared(map, head, white)
#endif
    for (int i=0; i<map->window->width; i++) {
        for (int j=0; j<map->window->height; j++) {
            f32 angle1 = (f32) i / (f32) map->window->width * (f32) M_PI * 2;
            f32 angle2 = (f32) j / (f32) map->window->height * (f32) M_PI * 2;
            f32 x = head->scale * cosf(angle1) + head->next->scale * cosf(angle2);
            f32 y = head->scale * sinf(angle1) + head->next->scale * sinf(angle2);
            if (x < -1 || x > 1 || y < -1 || y > 1)
                memcpy(map->data + 3*(i + j * map->window->width), white, 3 * sizeof(GLfloat));
        }
    }

    if (!buffer.index) map_save_texture(map);
#ifdef omp
#pragma omp parallel for default(none) shared(buffer, map, head)
#endif
    for (int i=0; i<buffer.index; i++) {
        map_add_block(map, head, buffer.data[i]);
    }
}

void map_add_block(map_t *map, joint_t *head, block_t block)
{
    for (int i=0; i<map->window->width; i++) {
        for (int j=0; j<map->window->height; j++) {
            f32 angle1 = (f32) i / (f32) map->window->width * (f32) M_PI * 2;
            f32 angle2 = (f32) j / (f32) map->window->height * (f32) M_PI * 2;
            f32 x = head->scale * cosf(angle1) + head->next->scale * cosf(angle2);
            f32 y = head->scale * sinf(angle1) + head->next->scale * sinf(angle2);
            if (x >= block.x - block.w && x < block.x + block.w && y >= block.y - block.w && y < block.y + block.w)
//            if (x*x+y*y<1)
                memcpy(map->data + 3*(i + j * map->window->width), &block.r, 3 * sizeof(GLfloat));
        }
    }
    map_save_texture(map);
}

map_t * map_create()
{
    map_t *map = callocate(sizeof(map_t));

    if (!(map->window = window_create(400, 400, TRUE))) return NULL;
    window_gl_acquire(map->window);
    if (!(map->renderer = renderer_create(
            "/shaders/map.vs",
            "/shaders/map.fs"
    ))) return NULL;

    if (!(map->cross = cross_create())) return NULL;

    map->data = allocate(3 * sizeof *map->data * map->window->width * map->window->height);

    GLint square[] = {
            -1, -1, 1, -1, 1, 1, -1, 1
    };

    glBindVertexArray(map->renderer->vao);
    glActiveTexture(GL_TEXTURE0);

    glGenTextures(1, &map->texture);
    glBindTexture(GL_TEXTURE_2D, map->texture);
    glTexImage2D(
            GL_TEXTURE_2D, 0, GL_RGB,
            map->window->width,map->window->height,
            0, GL_RGB, GL_UNSIGNED_BYTE, NULL
    );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glBindTexture(GL_TEXTURE_2D, 0);

    glBindBuffer(GL_ARRAY_BUFFER, map->renderer->vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof square, square, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glEnableVertexAttribArray(0);
    glVertexAttribIFormat(0, 2, GL_INT, 0);
    glVertexAttribBinding(0, 0);

    window_gl_release(map->window);
    return map;
}

void map_destroy(map_t *map)
{
    deallocate(map->data);
    cross_destroy(map->cross);
    window_gl_acquire(map->window);
    if (map->renderer) renderer_destroy(map->renderer);
    if (map->window) window_destroy(map->window);
    window_gl_release(map->window);
    deallocate(map);
}

void map_draw(map_t *map, joint_t *head)
{
    window_gl_acquire(map->window);
    glViewport(0, 0, map->window->width, map->window->height);
    glBindVertexArray(map->renderer->vao);
    glBindVertexBuffer(0, map->renderer->vbo, 0, 2 * sizeof(GLint));
    glUseProgram(map->renderer->shader_program->program);
    glBindTexture(GL_TEXTURE_2D, map->texture);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindVertexArray(0);
    glUseProgram(0);
    cross_draw(map->cross, head->angle / (f32) M_PI - 1, head->next->angle / (f32) M_PI - 1);
    window_swap(map->window);
    window_gl_release(map->window);
}