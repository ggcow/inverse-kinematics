#include "renderer.h"

renderer_t * renderer_create(const char *vs_path, const char *fs_path)
{
	renderer_t *renderer = callocate(sizeof(renderer_t));
    if (!(renderer->shader_program = shader_program_create(vs_path, fs_path))) return NULL;

    glGenVertexArrays(1, &(renderer->vao));
    glGenBuffers(1, &(renderer->vbo));
    glBindBuffer(GL_ARRAY_BUFFER, renderer->vbo);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glClearColor(0, 0, 0, 1);

//    glEnable(GL_DEPTH_TEST);
//    glDepthFunc(GL_LESS);
    glFrontFace(GL_CW);
    glCullFace(GL_FRONT);
    glEnable(GL_CULL_FACE);

	log_debug("Renderer created");

	return renderer;
}

void renderer_destroy(renderer_t *renderer)
{
    glDisableVertexArrayAttrib(renderer->vao, 0);
    glDisableVertexArrayAttrib(renderer->vao, 1);
    glDeleteBuffers(1, &renderer->vbo);
    glDeleteVertexArrays(1, &renderer->vao);
    if (renderer->shader_program) shader_program_destroy(renderer->shader_program);
	deallocate(renderer);
	log_debug("Renderer destroyed");
}


