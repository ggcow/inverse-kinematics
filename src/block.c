#include "block.h"
#include "opengl.h"

void block_setup(renderer_t *renderer)
{
    GLint square[] = {
            -1, -1, 1, -1, 1, 1, -1, 1
    };

    glBindVertexArray(renderer->vao);
    glBindBuffer(GL_ARRAY_BUFFER, renderer->vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof square + 100 * sizeof(block_t), NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof square, square);
    glEnableVertexAttribArray(0);
    glVertexAttribIFormat(0, 2, GL_INT, 0);
    glVertexAttribBinding(0, 0);
    glVertexBindingDivisor(0, 0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glVertexAttribFormat(1, 3, GL_FLOAT, GL_FALSE, 0);
    glVertexAttribFormat(2, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat));
    glVertexAttribBinding(1, 1);
    glVertexAttribBinding(2, 1);
    glVertexBindingDivisor(1, 1);

}

void block_draw(renderer_t *renderer, i32 count)
{
    glBindVertexArray(renderer->vao);
    glBindVertexBuffer(0, renderer->vbo, 0, 2 * sizeof(GLint));
    glBindVertexBuffer(1, renderer->vbo, 8 * sizeof(GLint), 6 * sizeof(GLfloat));
    glUseProgram(renderer->shader_program->program);
    glDrawArraysInstanced(GL_TRIANGLE_FAN, 0, 4, count);
    glBindVertexArray(0);
    glUseProgram(0);
}

void block_add(renderer_t *renderer, block_t block, i32 count)
{
    glNamedBufferSubData(
            renderer->vbo,
            8 * (int) sizeof(GLint) + count * (int) sizeof(block_t),
            sizeof(block_t),
            &block
    );
    log_info("bock added");
}