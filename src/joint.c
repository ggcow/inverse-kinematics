#include "joint.h"
#include "opengl.h"
#include <math.h>
#include <buffer.h>

void joint_draw(renderer_t *renderer, joint_t *head)
{
    glBindVertexArray(renderer->vao);
    glUseProgram(renderer->shader_program->program);
    int count;
    f32 tx = 0, ty = 0;
    for (count = 0; head; head = head->next, count++) {
        f32 scale = head->scale;
        f32 angle = head->angle;
        GLfloat matrix[] = {
                scale * cosf(angle), scale * sinf(angle),
                scale * -sinf(angle), scale * cosf(angle),
                tx, ty
        };
        tx += scale*cosf(angle);
        ty += scale*sinf(angle);
        glNamedBufferSubData(renderer->vbo, 8 * sizeof(GLfloat) + count * sizeof matrix, sizeof matrix, matrix);
    }
    glBindVertexBuffer(0, renderer->vbo, 0, 2 * sizeof(GLfloat));
    glBindVertexBuffer(1, renderer->vbo, sizeof(GLfloat) * 8, 6 * sizeof(GLfloat));
    glDrawArraysInstanced(GL_TRIANGLE_FAN, 0, 4, count);
    glBindVertexArray(0);
    glUseProgram(0);
}

void joint_setup(renderer_t *renderer)
{
    GLfloat triangle[] = {
            .5f, -0.05f, 1, 0, .5f, .05f, 0, 0
    };

    glBindVertexArray(renderer->vao);

    glBindBuffer(GL_ARRAY_BUFFER, renderer->vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof triangle + 12 * sizeof(f32), triangle, GL_DYNAMIC_DRAW);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);

    glVertexAttribFormat(0, 2, GL_FLOAT, GL_FALSE, 0);
    glVertexAttribBinding(0, 0);
    glVertexBindingDivisor(0, 0);
    glVertexBindingDivisor(1, 1);
    glVertexAttribFormat(1, 2, GL_FLOAT, GL_FALSE, 0);
    glVertexAttribBinding(1, 1);
    glVertexAttribFormat(2, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(f32));
    glVertexAttribBinding(2, 1);
    glVertexAttribFormat(3, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(f32));
    glVertexAttribBinding(3, 1);
}