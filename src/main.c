#include "common.h"
#include "window.h"
#include "renderer.h"
#include "joint.h"
#include <pthread.h>
#include <stdlib.h>
#include "map.h"
#include "event.h"

#define MAP

#ifndef MAP
#define window_gl_release
#define window_gl_acquire
#endif

#define APPROXIMATIONS_PER_FRAME 5
#define APPROXIMATION_COEFFICIENT .10f

#define RAND ((f32) (rand() % RAND_MAX) / RAND_MAX)

static void inverse_kinematics(joint_t *t, f32 tx, f32 ty);

int main() {
    srand(time(NULL));
    window_t *window = NULL;
    renderer_t *renderer = NULL;

    if (!(window = window_create(800, 600, TRUE))) goto error;
    window_gl_acquire(window);
    if (!(renderer = renderer_create(
            "/shaders/joint.vs",
            "/shaders/joint.fs"
    ))) goto error;
    joint_setup(renderer);
    window_gl_release(window);

#ifdef MAP
    renderer_t *block_renderer = NULL;
    map_t *map = NULL;

    i32 block_count = 0;

    window_gl_acquire(window);
    if (!(block_renderer = renderer_create(
            "/shaders/block.vs",
            "/shaders/block.fs"
    ))) goto error;
    block_setup(block_renderer);
    window_gl_release(window);
    if (!(map = map_create())) goto error;

    buffer_block_t block_buffer;
    buffer_init(block_buffer);
#endif

    joint_t t2 = {1.f/sqrtf(2) -.2f, 3.15f/4};
    joint_t t1 = {1.f/sqrtf(2) +.2f, 3.14f/4, &t2};

    map_refresh(map, &t1, block_buffer);

    i32 mouse_x, mouse_y;
    event_t event;
    do {
        event = poll_events(window, &mouse_x, &mouse_y);
        f32 tx = (f32) mouse_x / (f32) window->width * 2.f - 1;
        f32 ty = - (f32) mouse_y / (f32) window->height * 2.f + 1;
        for (int i=0; i<APPROXIMATIONS_PER_FRAME; i++) inverse_kinematics(&t1, tx, ty);

        window_gl_acquire(window);
        glViewport(0, 0, window->width, window->height);
        glClear(GL_COLOR_BUFFER_BIT);
#ifdef MAP
        block_draw(block_renderer, block_count);
#endif
        joint_draw(renderer, &t1);
        window_swap(window);
        window_gl_release(window);

#ifdef MAP
        if (event.action == EVENT_MAP && event.window_id == window->id) {
            block_t block = {tx, ty, .1f, RAND, RAND, RAND};
            buffer_check_size(block_buffer, 1);
            buffer_push(block_buffer, block);
            window_gl_acquire(window);
            block_add(block_renderer, block, block_count++);
            window_gl_release(window);
            map_add_block(map, &t1, block);
        }

        if (event.action == EVENT_P || event.action == EVENT_M) {
            t1.scale += event.action == EVENT_P ? .01f : -.01f;
            t2.scale += event.action == EVENT_P ? -.01f : .01f;
            map_refresh(map, &t1, block_buffer);
        }

        if (event.action == EVENT_WINDOW) {
            window_refresh_size(window);
            window_refresh_size(map->window);
            map_resize(map);
            map_refresh(map, &t1, block_buffer);
        }

        map_draw(map, &t1);
#endif
    } while (event.action != EVENT_QUIT);

    error:
#ifdef MAP
    if (map) map_destroy(map);
    window_gl_acquire(window);
    if (block_renderer) renderer_destroy(block_renderer);
    window_gl_release(window);
#endif
    window_gl_acquire(window);
    if (renderer) renderer_destroy(renderer);
    if (window) window_destroy(window);
    window_gl_release(window);
    opengl_error();
    return 0;
}



static float modulo(float a,float b)
{
    return a - b * floorf(a / b);
}

static void inverse_kinematics(joint_t *t, f32 tx, f32 ty)
{
    joint_t t1 = (joint_t) *t;
    joint_t t2 = (joint_t) *t->next;
    f32 h = .000001f;
    f32 d1x = (t1.scale*cosf(t1.angle+h)+t2.scale*cosf(t2.angle)
               - t1.scale*cosf(t1.angle-h)-t2.scale*cosf(t2.angle))/2/h;
    f32 d1y = (t1.scale*sinf(t1.angle+h)+t2.scale*sinf(t2.angle)
               - t1.scale*sinf(t1.angle-h)-t2.scale*sinf(t2.angle))/2/h;
    f32 d2x = (t1.scale*cosf(t1.angle)+t2.scale*cosf(t2.angle+h)
               - t1.scale*cosf(t1.angle)-t2.scale*cosf(t2.angle-h))/2/h;
    f32 d2y = (t1.scale*sinf(t1.angle)+t2.scale*sinf(t2.angle+h)
               - t1.scale*sinf(t1.angle)-t2.scale*sinf(t2.angle-h))/2/h;

    if (d1x*d2y == d2x*d1y) {
        t->angle += .1f;
        return;
    }
    f32 det = 1.f/(d1x*d2y-d2x*d1y);

    f32 jac_inv[] = {
            det*d2y, -det*d1y,
            -det*d2x, det*d1x
    };

    f32 x = t1.scale*cosf(t1.angle)+t2.scale*cosf(t2.angle);
    f32 y = t1.scale*sinf(t1.angle)+t2.scale*sinf(t2.angle);
    f32 dx = tx - x, dy = ty - y;

    f32 res1 = jac_inv[0] * dx + jac_inv[1] * dy;
    f32 res2 = jac_inv[2] * dx + jac_inv[3] * dy;

    t1.angle += res1 * APPROXIMATION_COEFFICIENT;
    t2.angle += res2 * APPROXIMATION_COEFFICIENT;

    t1.angle = modulo(t1.angle, 2 * (f32) M_PI);
    t2.angle = modulo(t2.angle, 2 * (f32) M_PI);

    *t->next = t2;
    *t = t1;
}
