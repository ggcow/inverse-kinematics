#include "window.h"
#include "timer.h"
#include "opengl.h"
#include <pthread.h>

static pthread_mutex_t gl_mutex = PTHREAD_MUTEX_INITIALIZER;
static volatile window_t *current = NULL;
static volatile int count = 0;

static void update_metrics(window_t *window)
{
//    static u32 frame_count = 0;
//    static f64 frame_start_time = 0;

    window->time_ms = timer_time_ms();
    u64 time = timer_time_us();
    window->time_delta = time - window->time_us;
//    frame_count++;
//    if (timer_time_s() - frame_start_time > 1) {
//        info("FPS : %u\n", frame_count);
//        log_command("A");
//        frame_count = 0;
//        frame_start_time = timer_time_s();
//    }

    window->time_us = time;
}

window_t * window_create(i32 width, i32 height, bool resizeable)
{
	window_t *window = callocate(sizeof(window_t));

	if (!SDL_WasInit(SDL_INIT_VIDEO)) {
		if (SDL_Init(SDL_INIT_VIDEO)) {
			log_error("SDL initialization failed: %s", SDL_GetError());
			deallocate(window);
			return NULL;
		} else {
			log_debug("SDL initialized");
		}
	}

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);

	window->sdl_window = SDL_CreateWindow(
		"OpenGL",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		width, height,
		SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | (resizeable * SDL_WINDOW_RESIZABLE)
	);

	if (window->sdl_window == NULL) {
		log_error("Window creation failed: %s", SDL_GetError());
		deallocate(window);
		return NULL;
	}
	log_debug("Window created");
    window->id = SDL_GetWindowID(window->sdl_window);

//	if (SDL_SetRelativeMouseMode(1)) {
//		log_error("Couldn't use SDL_CaptureMouse");
//	}

	window->gl_context = SDL_GL_CreateContext(window->sdl_window);
	if (window->gl_context == NULL) {
		log_error("OpenGL context creation failed: %s", SDL_GetError());
		deallocate(window);
		return NULL;
	}
    log_debug("OpenGL context created: %s", glGetString(GL_VERSION));

    update_metrics(window);

	window->width = width;
	window->height = height;

//    glViewport(0, 0, width, height);

	SDL_GL_SetSwapInterval(1);
    __atomic_add_fetch(&count, 1, __ATOMIC_RELAXED);

	return window;
}

void window_destroy(window_t *window) {
    SDL_GL_DeleteContext(window->gl_context);
    log_debug("Context destroyed");

    SDL_DestroyWindow(window->sdl_window);
    log_debug("Window destroyed");

    if (!--count) {
        SDL_Quit();
        log_debug("SDL uninitialized");
    }

	deallocate(window);
}

void window_swap(window_t *window)
{
	SDL_GL_SwapWindow(window->sdl_window);
    update_metrics(window);
}

void window_gl_acquire(window_t *window)
{
    volatile window_t *tmp_current = __atomic_load_n(&current, __ATOMIC_RELAXED);
    if (tmp_current == window) return;
    pthread_mutex_lock(&gl_mutex);
    tmp_current = window;
    __atomic_store(&current, &tmp_current, __ATOMIC_RELAXED);
    SDL_GL_MakeCurrent(window->sdl_window, window->gl_context);
}

void window_gl_release(window_t *window)
{
    volatile window_t *tmp_current = __atomic_load_n(&current, __ATOMIC_RELAXED);
    if (tmp_current != window) return;
    pthread_mutex_unlock(&gl_mutex);
}

f64 window_get_time_delta(const window_t *window)
{
	return (f64) window->time_delta;
}

u64 window_get_time_ms(const window_t *window)
{
	return window->time_ms;
}

f64 window_get_time_s(const window_t *window)
{
	return window->time_s;
}

void window_refresh_size(window_t *window)
{
    SDL_GetWindowSize(
            window->sdl_window,
            &window->width,
            &window->height
    );
}