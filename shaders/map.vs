#version 420

layout(location = 0) in ivec2 position;

out vec2 uv;

void main() {
    uv = vec2(position+1)/2;
    gl_Position = vec4(position, 0, 1);
}
