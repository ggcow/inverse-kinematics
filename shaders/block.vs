#version 330

layout(location = 0) in ivec2 position;
layout(location = 1) in vec3 data;
layout(location = 2) in vec3 color_in;

flat out vec3 color;

void main() {
    gl_Position = vec4(data.z * position + data.xy, 0, 1);
    color = color_in;
}
