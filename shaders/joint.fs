#version 330

out vec3 color;
flat in int id;

void main() {
    color = vec3(0, id, 1-id);
}
