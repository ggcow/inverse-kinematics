#version 440

layout(location = 0) in vec2 vertex_position;
layout(location = 1) uniform vec2 offset;

void main(){
    gl_Position = vec4(vertex_position+offset, 0, 1);
}