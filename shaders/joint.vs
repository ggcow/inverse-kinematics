#version 330

layout(location = 0) in vec2 position;
layout(location = 1) in mat3x2 mat;

flat out int id;

void main() {
    gl_Position = vec4(mat * vec3(position, 1), 0, 1);
    id = gl_InstanceID;
}
