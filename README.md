#### Inverse kinematic

This project's purpose is to show how to use the Jacobian inverse technique with 2 angles on a plan
The map axis are the 2 angles from 0 to 2PI.


How to build :
* cmake .
* make

Controls :
* Click : add a square
* M and P : change arm length


##### [Video link](https://youtu.be/dglOfJGSTMY)
##### [Video with map](https://youtu.be/DKkqfnxm8co)
