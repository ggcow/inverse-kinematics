#ifndef BOB_SHADER_H
#define BOB_SHADER_H

#include "opengl.h"


typedef struct shader_program_t {
    GLuint program;
    GLuint vertex_shader;
    GLuint fragment_shader;
} shader_program_t;

shader_program_t * shader_program_create(const char *vertex_shader_source,
                                         const char *fragment_shader_source);
void shader_program_destroy(shader_program_t *shader_program);

#endif
