#ifndef BOB_RENDERER_H
#define BOB_RENDERER_H

#include "common.h"
#include "opengl.h"
#include "shader.h"

typedef struct renderer_t {
    shader_program_t *shader_program;
	GLuint vao;
	GLuint vbo;
} renderer_t;

renderer_t * renderer_create();
void renderer_destroy(renderer_t *);

#endif