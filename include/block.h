#ifndef BOB_BLOCK_H
#define BOB_BLOCK_H

#include "common.h"
#include "buffer.h"
#include "renderer.h"

typedef struct block_t {
    GLfloat x, y, w;
    GLfloat r, g, b;
} block_t;

define_buffer_type(block_t);

void block_setup(renderer_t *renderer);
void block_draw(renderer_t *renderer, i32 count);
void block_add(renderer_t *renderer, block_t block, i32 count);

#endif //BOB_BLOCK_H
