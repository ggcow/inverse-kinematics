#ifndef BOB_TIMER_H
#define BOB_TIMER_H

#include "common.h"

u64 timer_time_us(void);
u64 timer_time_ms(void);
f64 timer_time_s(void);

#endif
