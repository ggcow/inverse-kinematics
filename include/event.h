#ifndef BOB_EVENT_H
#define BOB_EVENT_H

#include <limits.h>
#include "window.h"
#include "common.h"

typedef struct event_t {
    enum {
        EVENT_QUIT = INT32_MIN,
        EVENT_MAP,
        EVENT_P,
        EVENT_M,
        EVENT_WINDOW
    } action;
    u32 window_id;
} event_t;

event_t poll_events(window_t *window, i32 *x, i32 *y);

#endif //BOB_EVENT_H
