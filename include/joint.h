#ifndef BOB_JOINT_H
#define BOB_JOINT_H

#include "common.h"
#include "renderer.h"

typedef struct joint_t {
    f32 scale;
    f32 angle;
    struct joint_t *next;
} joint_t;

void joint_draw(renderer_t *renderer, joint_t *head);
void joint_setup(renderer_t *renderer);

#endif //BOB_JOINT_H
