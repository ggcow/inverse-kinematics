#ifndef BOB_WINDOW_H
#define BOB_WINDOW_H

#include "common.h"

#include <SDL2/SDL.h>

typedef struct window_t {
	SDL_Window *sdl_window;
    u32 id;
	SDL_GLContext *gl_context;
	i32 width, height;

	f64 time_s;
	u64 time_delta;
	u64 time_ms;
	u64 time_us;
} window_t;

window_t * window_create(i32 width, i32 height, bool resizeable);
void window_destroy(window_t *window);

void window_swap(window_t *window);
void window_gl_acquire(window_t *window);
void window_gl_release(window_t *window);

u64 window_get_time_ms(const window_t *window);
f64 window_get_time_s(const window_t *window);
f64 window_get_time_delta(const window_t *window);

void window_refresh_size(window_t *window);

#endif
