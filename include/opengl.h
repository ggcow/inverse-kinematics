#ifndef BOB_OPENGL_H
#define BOB_OPENGL_H

#define LINUX

#ifdef LINUX
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#endif

void _opengl_error(const char *filename, int line);

#define opengl_error(...) \
	_opengl_error(__FILE__, __LINE__)

#endif