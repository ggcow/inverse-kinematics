#ifndef BOB_CROSS_H
#define BOB_CROSS_H

#include "renderer.h"

typedef struct cross_t {
    renderer_t *renderer;
} cross_t;

cross_t * cross_create();
void cross_destroy(cross_t *cross);
void cross_draw(cross_t *cross, f32 x, f32 y);

#endif //BOB_CROSS_H
