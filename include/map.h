#ifndef BOB_MAP_H
#define BOB_MAP_H

#include "renderer.h"
#include "block.h"
#include "joint.h"
#include "window.h"
#include "cross.h"

typedef struct map_t {
    window_t *window;
    renderer_t *renderer;
    GLuint texture;
    GLfloat *data;
    cross_t *cross;
} map_t;

void map_resize(map_t *map);
void map_refresh(map_t *map, joint_t *head, buffer_block_t buffer);
void map_add_block(map_t *map, joint_t *head, block_t block);
map_t * map_create();
void map_draw(map_t *map, joint_t *head);
void map_destroy(map_t *map);

#endif //BOB_MAP_H
